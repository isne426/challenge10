#include <iostream>
#include "list.h"

using namespace std;

void main(){
	
	List list;
	int opt, data;
	
	do{
		cout << endl <<"Select the option by choose the following number. (Press 0 to end.)" << endl;
		cout << "1. Head push." << endl;
		cout << "2. Tail push." << endl;
		cout << "3. Head pop." << endl;
		cout << "4. Tail pop." << endl;
		cout << "5. Delete node." << endl;
		cout << "6. Finding a data." << endl << endl;
		cin >> opt;																										//Asking for the option of processing.

		switch (opt){
			
		case 1:
			cout << endl << "Enter your data : ";
			cin >> data;
			
			list.headPush(data);
			cout << "This is your list : ";
			
			list.display(data);
			cout << endl;
			
			break;
			
			
		case 2:
			cout << endl << "Enter your data : ";
			cin >> data;
			
			list.tailPush(data);
			cout << "This is your list : ";
			
			list.display(data);
			cout << endl;
			
			break;
			
		case 3:
			list.headPop();
			cout << endl << "This is your list : ";
			
			list.display(data);
			cout << endl;
			
			break;
			
		case 4:
			list.tailPop();
			cout << endl << "This is your list : ";
			
			list.display(data);
			cout << endl;
			
			break;
			
		case 5:
			cout << endl << "Which node do you want to delete? : ";
			cin >> data;
			
			list.deleteNode(data);
			
			break;
			
		case 6:
			cout << endl << "What is your data? : ";
			cin >> data;
			
			list.isInList(data);
			
			if (true) { 
			cout << data << " is in the list."; 
			}else { cout << data << "is not in the list."; 
			}
			
			cout << endl;
			break;
		}																						//Operation up to the option that was selected.
	} while (opt != 0);

	cout << endl << "This is your list : ";
	
	
	list.display(data);
	cout << endl;																				//Display the list.
	
	
	system("Pause");																			//Stop the process.
}
