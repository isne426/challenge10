#include <iostream>
#include "list.h"


using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ){
		p = head->next;
		delete head;
		head = p;
	}
}																									//starter

void List::headPush(int value){
																									
	Node *create = new Node(value, head, NULL);
	head = create;
	if (tail == 0){
		tail = head;
	}
}																									//Adding data to front of list/ setting the head and the node.

void List::tailPush(int value){
	
	Node *create = new Node(value, NULL, tail);
	{
		if (tail != 0){
			tail->next = create;
		}
		tail = create;
	}
}																									//Adding data to the tail.




int List::headPop(){
	
	if (head == 0){
		return 0;
	}

	int value = tail->info;
	if (tail == head){
		delete tail;
		tail = 0;
		head = 0;
	}else{
		Node *del = tail->prev;
		delete tail;
		tail = del;
		tail->next = NULL;
	}
																												//Remove and return data from front of list.
	return value;																								//Ruturn the value.
}


int List::tailPop(){
	
	if (tail == 0){
		return 0;
	}

	int value = tail->info;
	if (tail == head){
		delete tail;
		tail = 0;
		head = 0;
	}else{
		Node *del = tail->prev;
		delete tail;
		tail = del;
		tail->next = NULL;
	}

	return value;
}																												//Remove and return data from tail of list.


void List::deleteNode(int value)
{
	
	Node *del = head, *temporary1 = 0, *temporary2 = 0;
	if (head->info == value){
		
		headPop();
	}else if (tail->info == value){
		
		tailPop();
	}else{
		
		del = del->next;
		temporary1 = head;
		temporary2 = del->next;
		
		
		while (del != tail){
			if (del->info == value){
				
				temporary1->next = temporary2;
				temporary2->prev = temporary1;
				delete del;
				break;
			}
			temporary1 = del;
			del = temporary2;
			temporary2 = temporary2->next;
		}
	}
}																													//Delete a particular value.

bool List::isInList(int value){
	
	Node *index = head;
	while (index != NULL){
		
		if (index->info == value){
			
			return true;
		}
		index = index->next;
	}

	return false;
}																													//Check if a particular value is in the list.

void List::display(int){

	Node *index = head;
	while (index != NULL){
		cout << index->info << " ";
		index = index -> next;
	}
}																														//To show all data in the list.
